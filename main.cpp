#include <bits/stdc++.h>

using namespace std;

#define len(c) int((c).size())

#ifdef DAT
#endif

namespace Task {

enum Action { SHIFT, REDUCE, ACCEPT, REJECT };

struct Operation {
  vector<string> stack_state;
  int precedence;
  string input_state;
  string rule_state;
  Action action;
    Operation(){};
};

void solve() {
  map<string, vector<string>> rules = {{"S", {"Sa", "Sb", "Cb", "Da", "SCb", "SDa", "a", "b"}},
                                       {"C", {"A"}},
                                       {"D", {"B"}},
                                       {"A", {"Ac", "c"}},
                                       {"B", {"Bd", "d"}}};
  string order = "SCDABabcd";
  int N = len(order);
  cout<<"len: "<<N<<endl;
          vector<vector<int>> parsing_table(N + 2, vector<int>(N + 2, -1));
          for (int i = 0; i < N; ++i) {
            for (int j = 0; j < N; ++j) {
              cin >> parsing_table[i][j];
            }
          }
    /*
    * -1 - empty
    *  0 - equal
    *  1 - less
    *  2 - greater
    * */
  for (int j = 0; j < N; ++j) {
    parsing_table[N][j] = 1;
    parsing_table[j][N] = 2;
  }

  for (int i = 0; i <= N; ++i) {
    for (int j = 0; j <= N; ++j) {
        if(parsing_table[i][j]==-1)
            cout<<"- ";
        else if(parsing_table[i][j] == 0)
            cout<<"= ";
        else if(parsing_table[i][j] == 1)
            cout<<"< ";
        else
            cout<<"> ";
    }
    cout << '\n';
  }

  auto find_precedence = [&](const char &u, const char &v) -> int {
    if (u == '#') {
      return (v == '#' ? -1 : 1);
    } else if (v == '#') {
      return 2;
    }
    for (int i = 0; i < N; ++i) {
      for (int j = 0; j < N; ++j) {
        if (order[i] == u && order[j] == v) {
          return parsing_table[i][j];
        }
      }
    }
    return -1;
  };

  function < void(string) > simple_precedence_parser = [&](string input) -> void {
    input += "#";
    vector<string> stk;
    vector<Operation> list_operation;

    stk.emplace_back("#");
    bool parsing_state = true;
    while (parsing_state) {
      Operation cur_op = Operation();
      cur_op.stack_state = stk;
      cur_op.input_state = input;

      if (len(stk) == 2 && stk[0] == "#" && stk[1] == "S") {
        cur_op.action = ACCEPT;
        list_operation.emplace_back(cur_op);
        break;
      }

      string top = stk.back();
      string next = cur_op.input_state;
      cur_op.precedence = -1;
      if (top == "#") {
        cur_op.precedence = 1;
      }

      cur_op.precedence = find_precedence(top[0], next[0]);
      // debug(top[0],next[0]);
      if (cur_op.precedence == -1) {
        cur_op.action = REJECT;
        list_operation.emplace_back(cur_op);
        break;
      }

      if (cur_op.precedence < 2) {
        cur_op.action = SHIFT;
        stk.emplace_back(input.substr(0, 1));
        input.erase(0, 1);
        list_operation.emplace_back(cur_op);
      } else {
        bool check = false;
        while (len(stk) > 1) {
          string removed = stk.back();
          stk.pop_back();
          string new_top = stk.back();
          if (find_precedence(new_top[0], removed[0]) == 1) {
            string left = "";
            string right = "";
            for (auto const &[lhs, rhs_vec] : rules) {
              for (auto const &str : rhs_vec) {
                if (str[0] == removed[0] and find_precedence(lhs[0],next[0]) != -1) {
                  if (left == "" and right == "") {
                    left = lhs;
                    right = str;
                  }
                  break;
                }
              }
            }
            if (left != "") {
                check = true;
              stk.emplace_back(left);
              cur_op.action = REDUCE;
              cur_op.rule_state = left + " --> " + right;
              list_operation.emplace_back(cur_op);
            }
            break;
          }
        }
        if (!check) {
          parsing_state = false;
          cur_op.action = REJECT;
          list_operation.emplace_back(cur_op);
        }
      }
    }


    for (int i = 0; i < len(list_operation); ++i) {
      Operation cur_op = list_operation[i];
      string precedence =
          cur_op.precedence == 0 ? "=" : (cur_op.precedence == 1 ? "<" : ">");
      string rule_state = cur_op.rule_state == "" ? "-" : cur_op.rule_state;
      string input_state = cur_op.input_state;

      string stack_state = "";
      for (auto s : cur_op.stack_state) {
        stack_state += s;
      }

      string action =
          (cur_op.action == SHIFT ? "SHIFT"
          : (cur_op.action == REDUCE   ? "REDUCE"
          : cur_op.action == ACCEPT ? "SUCCESS!" : "FAILED!"));
      cout << action << '\n';
    }

    cout << " --------------------------------------------"
            "\n\n";
  };

  string input;
  while (cin >> input) {
    simple_precedence_parser(input);
  }

  //// CLEAR
  auto clearData = [&]() -> void {};
  clearData();
 }
} // namespace Task

int main(int argc, char **argv) {
  ios_base::sync_with_stdio(false);
  for (int i = 1; i <=1; i++) {
    Task::solve();
  }
}